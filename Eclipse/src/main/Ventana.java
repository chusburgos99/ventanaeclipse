package main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JToolBar;
import java.awt.Panel;
import java.awt.SystemColor;
import javax.swing.ButtonGroup;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.ImageIcon;

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField txtEmail;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_7;
	private JTextField textField_8;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setFont(new Font("Quicksand Medium", Font.PLAIN, 12));
		setTitle("Eclipse");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 594, 368);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 41, 558, 277);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Usuario", null, panel, null);
		panel.setLayout(null);
		
		JButton btnIniciarSesion = new JButton("Iniciar sesion");
		btnIniciarSesion.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		btnIniciarSesion.setBounds(70, 125, 108, 23);
		panel.add(btnIniciarSesion);
		
		txtEmail = new JTextField();
		txtEmail.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		txtEmail.setBounds(101, 63, 142, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		textField_3.setBounds(101, 94, 142, 20);
		panel.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		textField_4.setBounds(402, 63, 126, 20);
		panel.add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		textField_5.setBounds(402, 94, 126, 20);
		panel.add(textField_5);
		textField_5.setColumns(10);
		
		JButton btnRegistrarse = new JButton("Registrarse");
		btnRegistrarse.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		btnRegistrarse.setBounds(335, 218, 99, 23);
		panel.add(btnRegistrarse);
		
		textField_7 = new JTextField();
		textField_7.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		textField_7.setBounds(402, 156, 126, 20);
		panel.add(textField_7);
		textField_7.setColumns(10);
		
		textField_8 = new JTextField();
		textField_8.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		textField_8.setBounds(402, 187, 126, 20);
		panel.add(textField_8);
		textField_8.setColumns(10);
		
		JLabel lblIniciarSesin = new JLabel("INICIAR SESI\u00D3N");
		lblIniciarSesin.setForeground(Color.BLUE);
		lblIniciarSesin.setBackground(Color.WHITE);
		lblIniciarSesin.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblIniciarSesin.setBounds(70, 38, 90, 14);
		panel.add(lblIniciarSesin);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblEmail.setBounds(29, 66, 90, 14);
		panel.add(lblEmail);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a");
		lblContrasea.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblContrasea.setBounds(29, 97, 90, 14);
		panel.add(lblContrasea);
		
		JLabel lblRegistrarse = new JLabel("REGISTRARSE");
		lblRegistrarse.setForeground(Color.MAGENTA);
		lblRegistrarse.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblRegistrarse.setBounds(344, 38, 90, 14);
		panel.add(lblRegistrarse);
		
		JLabel lblNombre = new JLabel("Apellido");
		lblNombre.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblNombre.setBounds(284, 97, 90, 14);
		panel.add(lblNombre);
		
		JLabel lblNombre_1 = new JLabel("Nombre\r\n");
		lblNombre_1.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblNombre_1.setBounds(283, 66, 90, 14);
		panel.add(lblNombre_1);
		
		JLabel lblFechaNacimiento = new JLabel("Fecha nacimiento");
		lblFechaNacimiento.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblFechaNacimiento.setBounds(284, 129, 111, 14);
		panel.add(lblFechaNacimiento);
		
		JLabel label = new JLabel("Email");
		label.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		label.setBounds(284, 159, 90, 14);
		panel.add(label);
		
		JLabel label_1 = new JLabel("Contrase\u00F1a");
		label_1.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		label_1.setBounds(284, 187, 90, 14);
		panel.add(label_1);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerDateModel(new Date(1550012400000L), new Date(-315622800000L), null, Calendar.DAY_OF_YEAR));
		spinner.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		spinner.setBounds(405, 126, 123, 20);
		panel.add(spinner);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Coleccion", null, panel_1, null);
		panel_1.setLayout(null);
		
		JButton btnOtoo = new JButton("Oto\u00F1o");
		btnOtoo.setForeground(new Color(60, 179, 113));
		btnOtoo.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		btnOtoo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnOtoo.setBounds(31, 63, 76, 23);
		panel_1.add(btnOtoo);
		
		JButton btnInvierno = new JButton("Invierno");
		btnInvierno.setForeground(new Color(106, 90, 205));
		btnInvierno.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		btnInvierno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnInvierno.setBounds(124, 63, 89, 23);
		panel_1.add(btnInvierno);
		
		JButton btnPrimavera = new JButton("Primavera");
		btnPrimavera.setForeground(new Color(240, 128, 128));
		btnPrimavera.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		btnPrimavera.setBounds(223, 63, 89, 23);
		panel_1.add(btnPrimavera);
		
		JButton btnVerano = new JButton("Verano");
		btnVerano.setForeground(new Color(255, 165, 0));
		btnVerano.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		btnVerano.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnVerano.setBounds(322, 63, 81, 23);
		panel_1.add(btnVerano);
		
		JCheckBox chckbxColeccion = new JCheckBox("Coleccion");
		buttonGroup.add(chckbxColeccion);
		chckbxColeccion.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		chckbxColeccion.setBounds(31, 21, 97, 23);
		panel_1.add(chckbxColeccion);
		
		JCheckBox chckbxTendencias = new JCheckBox("Tendencias");
		buttonGroup.add(chckbxTendencias);
		chckbxTendencias.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		chckbxTendencias.setBounds(160, 21, 97, 23);
		panel_1.add(chckbxTendencias);
		
		JCheckBox chckbxRebajas = new JCheckBox("Rebajas");
		buttonGroup.add(chckbxRebajas);
		chckbxRebajas.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		chckbxRebajas.setBounds(292, 21, 97, 23);
		panel_1.add(chckbxRebajas);
		
		JPanel panel_5 = new JPanel();
		tabbedPane.addTab("Seguimiento", null, panel_5, null);
		panel_5.setLayout(null);
		
		JLabel lblSeguimientoDeSu = new JLabel("Seguimiento de su pedido:");
		lblSeguimientoDeSu.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblSeguimientoDeSu.setBounds(21, 11, 152, 14);
		panel_5.add(lblSeguimientoDeSu);
		
		JLabel lblNewLabel_4 = new JLabel("");
		lblNewLabel_4.setIcon(new ImageIcon(Ventana.class.getResource("/Imagenes/Mapa.PNG")));
		lblNewLabel_4.setBounds(10, 11, 533, 227);
		panel_5.add(lblNewLabel_4);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Metodo pago", null, panel_2, null);
		panel_2.setLayout(null);
		
		JCheckBox chckbxVisa = new JCheckBox("VISA");
		chckbxVisa.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		chckbxVisa.setBounds(32, 28, 97, 23);
		panel_2.add(chckbxVisa);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("VISA ELECTRONICA");
		chckbxNewCheckBox.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		chckbxNewCheckBox.setBounds(32, 54, 136, 23);
		panel_2.add(chckbxNewCheckBox);
		
		JCheckBox chckbxMastercard = new JCheckBox("MasterCard");
		chckbxMastercard.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		chckbxMastercard.setBounds(32, 80, 97, 23);
		panel_2.add(chckbxMastercard);
		
		JCheckBox chckbxNewCheckBox_1 = new JCheckBox("American EXPRESS");
		chckbxNewCheckBox_1.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		chckbxNewCheckBox_1.setBounds(32, 103, 136, 23);
		panel_2.add(chckbxNewCheckBox_1);
		
		textField = new JTextField();
		textField.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		textField.setBounds(273, 54, 121, 20);
		panel_2.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		textField_1.setBounds(273, 156, 121, 20);
		panel_2.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		textField_2.setBounds(273, 104, 121, 20);
		panel_2.add(textField_2);
		textField_2.setColumns(10);
		
		JButton btnFinalizarElPago = new JButton("Finalizar el pago");
		btnFinalizarElPago.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		btnFinalizarElPago.setBounds(273, 199, 121, 23);
		panel_2.add(btnFinalizarElPago);
		
		JLabel lblNumeroVisa = new JLabel("Numero VISA");
		lblNumeroVisa.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblNumeroVisa.setBounds(273, 32, 82, 14);
		panel_2.add(lblNumeroVisa);
		
		JLabel lblCaducidad = new JLabel("Caducidad");
		lblCaducidad.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblCaducidad.setBounds(273, 84, 82, 14);
		panel_2.add(lblCaducidad);
		
		JLabel lblNumeroSecreto = new JLabel("Numero secreto");
		lblNumeroSecreto.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblNumeroSecreto.setBounds(273, 135, 104, 14);
		panel_2.add(lblNumeroSecreto);
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Devoluciones", null, panel_3, null);
		panel_3.setLayout(null);
		
		JButton btnRealizarDevoluci = new JButton("Realizar devolucion");
		btnRealizarDevoluci.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		btnRealizarDevoluci.setBounds(223, 107, 142, 23);
		panel_3.add(btnRealizarDevoluci);
		
		JSlider slider = new JSlider();
		slider.setValue(5);
		slider.setMajorTickSpacing(1);
		slider.setMaximum(10);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setBounds(343, 198, 200, 40);
		panel_3.add(slider);
		
		JLabel lblNewLabel = new JLabel("Cuando la prenda llegue a su destino, el cliente tiene 48 / 72 horas \r\n");
		lblNewLabel.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblNewLabel.setBounds(33, 24, 374, 23);
		panel_3.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("para la posible devoluci\u00F3n, entregando el paquete de la m\u00E1s parecida\r\n");
		lblNewLabel_1.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblNewLabel_1.setBounds(33, 45, 374, 14);
		panel_3.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("posible como el cliente la ha recibido, se reembiara a la direccion,\r\n");
		lblNewLabel_2.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblNewLabel_2.setBounds(33, 58, 374, 22);
		panel_3.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("desde la oficina mas cercana de correos.\r\n");
		lblNewLabel_3.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblNewLabel_3.setBounds(33, 82, 332, 14);
		panel_3.add(lblNewLabel_3);
		
		JLabel lblDanosTuOpinin = new JLabel("Danos tu opini\u00F3n");
		lblDanosTuOpinin.setFont(new Font("Quicksand Medium", Font.PLAIN, 11));
		lblDanosTuOpinin.setBounds(386, 173, 105, 14);
		panel_3.add(lblDanosTuOpinin);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(453, 14, 103, 23);
		toolBar.setForeground(SystemColor.activeCaption);
		toolBar.setFont(new Font("Quicksand Medium", Font.PLAIN, 12));
		contentPane.add(toolBar);
		
		JButton btnCerrarSesin = new JButton("Cerrar sesi\u00F3n");
		btnCerrarSesin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		toolBar.add(btnCerrarSesin);
	}
}
